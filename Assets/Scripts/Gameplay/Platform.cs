﻿using UnityEngine;

namespace LunarLander
{
    public class Platform : MonoBehaviour
    {
        public int multiplyPoints = 1;

        [SerializeField] private TextMesh pointsText;

        public void UpdatePoints(int mPoints)
        {
            multiplyPoints = mPoints;
            pointsText.text = "x" + mPoints;
        }
    }
}