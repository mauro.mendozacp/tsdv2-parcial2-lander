﻿using System.Collections;
using UnityEngine;

namespace LunarLander
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] public PlayerData data;
        [SerializeField] private Vector2 startVelocity;
        [SerializeField] private Vector2 landingVelocity;
        [SerializeField] public LayerMask landingMask;
        [SerializeField] public LayerMask platformMask;
        [SerializeField] private GameObject fire;
        [SerializeField] private float zoomDetectionRadius;
        [SerializeField] private AudioClip bombSound;

        Animator anim;
        AudioSource rocketAudio;
        ParticleSystem firePS;
        Vector2 lastVelocity;
        float maxDistance = 1000f;
        bool collidered = false;
        float waitFinishScreenSeconds = 1f;
        float minAngleTerrain = 8f;

        void Start()
        {
            data.rigid = GetComponent<Rigidbody2D>();
            anim = GetComponent<Animator>();
            firePS = fire.GetComponent<ParticleSystem>();
            rocketAudio = GetComponent<AudioSource>();

            PlayerData.receiveScoreEvent?.Invoke(data.Score);
            PlayerData.receiveAltitudeEvent?.Invoke(data.Altitude);
            PlayerData.receiveVelocityHEvent?.Invoke(data.VelocityH);
            PlayerData.receiveVelocityVEvent?.Invoke(data.VelocityV);
            data.Fuel = data.fuelBase;

            data.groundCheckDistance = GetComponent<CapsuleCollider2D>().bounds.size.y / 2 + 0.05f;
            data.rigid.AddForce(startVelocity, ForceMode2D.Impulse);
            data.rigid.gravityScale = data.gravity;
            rocketAudio.loop = true;
        }

        private void LateUpdate()
        {
            if (!collidered)
            {
                data.Altitude = Physics2D.Raycast(transform.position, Vector2.down, maxDistance, landingMask).distance;
                data.VelocityH = data.rigid.velocity.x * 10f;
                data.VelocityV = data.rigid.velocity.y * 10f;
            }
        }

        void FixedUpdate()
        {
            if (!collidered)
            {
                Move();
                Rotate();

                lastVelocity = data.rigid.velocity;
            }
        }

        void Move()
        {
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.Space))
            {
                Vector2 force = transform.up * data.moveSpeed;
                data.rigid.AddForce(force, ForceMode2D.Force);

                data.Fuel -= Time.deltaTime;

                if (!rocketAudio.isPlaying)
                {
                    rocketAudio.Play();
                }

                if (!firePS.isEmitting)
                {
                    firePS.Play();
                }
            }
            else
            {
                if (rocketAudio.isPlaying)
                {
                    rocketAudio.Stop();
                }

                if (!firePS.isStopped)
                {
                    firePS.Stop();
                }
            }
        }

        void Rotate()
        {
            float axisX = Input.GetAxis("Horizontal");
            if (axisX > Mathf.Epsilon || axisX < Mathf.Epsilon)
            {
                Vector3 rot = transform.localEulerAngles;
                rot.z += -(axisX * data.turnSpeed);

                if (rot.z < 90f || rot.z > 270f)
                {
                    transform.localEulerAngles = rot;
                }
            }
        }

        bool CheckGroundFloor()
        {
            return Physics2D.Raycast(transform.position, -transform.up, data.groundCheckDistance, landingMask).collider != null;
        }

        void UpdateMultplyPoints()
        {
            data.multiplyPoints = 1;

            Collider2D collider2d = Physics2D.Raycast(transform.position, -transform.up, data.groundCheckDistance, platformMask).collider;
            if (collider2d != null)
            {
                data.multiplyPoints = collider2d.GetComponent<Platform>().multiplyPoints;
            }
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            if (!collidered)
            {
                rocketAudio.Stop();
                collidered = true;
                fire.SetActive(false);
                data.rigid.constraints = RigidbodyConstraints2D.FreezeAll;

                if (CheckGroundFloor())
                {
                    Vector2 vel = new Vector2(Mathf.Abs(lastVelocity.x), Mathf.Abs(lastVelocity.y));
                    if (Vector2.Min(vel, landingVelocity) == vel)
                    {
                        if (transform.localEulerAngles.z < minAngleTerrain || transform.localEulerAngles.z > 360f - minAngleTerrain)
                        {
                            UpdateMultplyPoints();
                            StartCoroutine(FinishScreen(true));
                            return;
                        }
                    }
                }

                rocketAudio.loop = false;
                rocketAudio.clip = bombSound;
                rocketAudio.Play();
                StartCoroutine(FinishScreen(false));
            }
        }

        public void Fail()
        {
            anim.SetTrigger("Destroy");
            GameplayManager.Get().FinishLevel(false);
        }

        IEnumerator FinishScreen(bool win)
        {
            if (!win)
            {
                anim.SetTrigger("Destroy");
            }

            yield return new WaitForSeconds(waitFinishScreenSeconds);

            GameplayManager.Get().FinishLevel(win);
        }

        public bool ZoomDetection()
        {
            return Physics2D.OverlapCircle(transform.position, zoomDetectionRadius, landingMask) != null;
        }
    }
}