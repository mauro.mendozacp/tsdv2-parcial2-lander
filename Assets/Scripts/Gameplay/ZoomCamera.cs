﻿using UnityEngine;

namespace LunarLander
{
    public class ZoomCamera : MonoBehaviour
    {
        [SerializeField] private PlayerController player;
        [SerializeField] private float zoomSize;
        [SerializeField] private float speed;

        Camera mainCamera;
        Vector3 originPos;
        float zoomSizeOrigin;
        float offset = 5f;
        float minPosX, maxPosX, minPosY;

        void Start()
        {
            mainCamera = GetComponent<Camera>();
            originPos = transform.position;
            zoomSizeOrigin = mainCamera.orthographicSize;

            minPosX = originPos.x - zoomSizeOrigin + offset;
            maxPosX = originPos.x + zoomSizeOrigin - offset;
            minPosY = originPos.y - (zoomSizeOrigin / 2) + (offset / 2);
        }

        void Update()
        {
            Zoom();
        }

        public void Zoom()
        {
            Vector3 cameraPos = transform.position;
            Vector3 playerPos = GetPositionPlayer();

            if (player.ZoomDetection())
            {
                transform.position = Vector3.MoveTowards(cameraPos, playerPos, speed);
                mainCamera.orthographicSize = Mathf.MoveTowards(mainCamera.orthographicSize, zoomSize, speed);
            }
            else
            {
                transform.position = Vector3.MoveTowards(cameraPos, originPos, speed);
                mainCamera.orthographicSize = Mathf.MoveTowards(mainCamera.orthographicSize, zoomSizeOrigin, speed / 2);
            }
        }

        public Vector3 GetPositionPlayer()
        {
            Vector3 playerPos = player.transform.position;

            if (playerPos.x < minPosX)
            {
                playerPos.x = minPosX;
            }
            if (playerPos.x > maxPosX)
            {
                playerPos.x = maxPosX;
            }
            if (playerPos.y < minPosY)
            {
                playerPos.y = minPosY;
            }

            playerPos.z = transform.position.z;

            return playerPos;
        }
    }
}