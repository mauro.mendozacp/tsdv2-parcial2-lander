﻿using UnityEngine;
using UnityEngine.U2D;

namespace LunarLander
{
    public class WindManager : MonoBehaviour
    {
        [SerializeField] private GameObject windPrefab;
        [SerializeField] private int windCount = 2;

        float offsetY = 2f;

        SpriteShapeController shape;

        public void InitWinds()
        {
            shape = GameplayManager.Get().tGenerate.shape;

            bool repeat = false;
            int index = 0;
            for (int i = 0; i < windCount; i++)
            {
                repeat = false;
                index = Random.Range(4, shape.spline.GetPointCount() - 5);

                foreach (int pIndex in GameplayManager.Get().platformIndex)
                {
                    if (index == pIndex)
                    {
                        repeat = true;
                        break;
                    }
                }

                if (!repeat)
                {
                    GameplayManager.Get().platformIndex.Add(index);

                    GameObject windGO = Instantiate(windPrefab);
                    windGO.name = "Wind " + (i + 1);

                    Vector3 auxPos = GameplayManager.Get().tGenerate.transform.position + shape.spline.GetPosition(index);
                    auxPos.y += offsetY;

                    windGO.transform.position = auxPos;
                    windGO.transform.parent = transform;

                    GameplayManager.Get().tGenerate.ChangeShapePosition(index, index + 1);
                    GameplayManager.Get().tGenerate.ChangeShapePosition(index, index - 1);
                }
            }
        }
    }
}