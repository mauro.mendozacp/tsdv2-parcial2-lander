﻿using System.Collections.Generic;
using UnityEngine;

namespace LunarLander
{
    public class GameplayManager : MonoBehaviourSingleton<GameplayManager>
    {
        [SerializeField] private GameObject pauseGO;
        [SerializeField] private GameObject finishLevelGO;
        [SerializeField] private PlayerController player;
        [SerializeField] private PlatformManager platformManager;
        [SerializeField] private WindManager windManager;
        [SerializeField] private int scoreBase = 100;
        public TerrainGenerate tGenerate;

        [HideInInspector] public float timer;
        [HideInInspector] public bool pause;
        [HideInInspector] public List<int> platformIndex = new List<int>();        

        public override void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
        }

        void Start()
        {
            platformManager.InitPlatforms();

            if (GameManager.Get().Level > 1)
            {
                windManager.InitWinds();
            }

            player.data.Score = GameManager.Get().Score;

            if (GameManager.Get().Level != 1)
            {
                player.data.Fuel = GameManager.Get().Fuel;
            }
        }

        void Update()
        {
            if (!GameManager.Get().Finish)
            {
                timer += Time.deltaTime;
            }
        }

        public void PauseGame()
        {
            pause = true;
            Time.timeScale = 0f;
            pauseGO.SetActive(true);
        }

        public void FinishLevel(bool win)
        {
            GameManager.Get().FinishLevel(player.data, win);

            if (win)
            {
                GameManager.Get().Score += scoreBase * player.data.multiplyPoints;
            }

            finishLevelGO.GetComponent<FinishLevelUI>().SetInfo();
            finishLevelGO.SetActive(true);
        }
    }
}