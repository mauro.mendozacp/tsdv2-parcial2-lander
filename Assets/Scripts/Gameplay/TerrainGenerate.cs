﻿using UnityEngine;
using UnityEngine.U2D;

namespace LunarLander
{
    public class TerrainGenerate : MonoBehaviour
    {
        [SerializeField] private float minHeight;
        [SerializeField] private float maxHeight;
        [SerializeField] private int scale;
        [SerializeField] private int pointsCount;

        [HideInInspector] public SpriteShapeController shape;

        void Awake()
        {
            shape = GetComponent<SpriteShapeController>();

            float distancePoints = scale / pointsCount;

            for (int i = 0; i < pointsCount; i++)
            {
                float xPos = shape.spline.GetPosition(i + 1).x + distancePoints;
                shape.spline.InsertPointAt(i + 2, new Vector3(xPos, minHeight * Mathf.PerlinNoise(i * Random.Range(minHeight, maxHeight), 0)));
            }

            for (int i = 2; i < shape.spline.GetPointCount() - 2; i++)
            {
                shape.spline.SetTangentMode(i, ShapeTangentMode.Continuous);
                shape.spline.SetLeftTangent(i, Vector3.left);
                shape.spline.SetRightTangent(i, Vector3.right);
            }
        }

        public void ChangeShapePosition(int index, int nextIndex)
        {
            if (shape.spline.GetPosition(index).y < shape.spline.GetPosition(nextIndex).y)
            {
                bool repeat = false;
                foreach (int pIndex in GameplayManager.Get().platformIndex)
                {
                    if (nextIndex == pIndex)
                    {
                        repeat = true;
                        break;
                    }
                }

                if (!repeat)
                {
                    Vector3 pos2 = shape.spline.GetPosition(nextIndex);
                    pos2.y = shape.spline.GetPosition(index).y;
                    shape.spline.SetPosition(nextIndex, pos2);
                }
            }
        }
    }
}