﻿using UnityEngine;
using UnityEngine.U2D;

namespace LunarLander
{
    public class PlatformManager : MonoBehaviour
    {
        [SerializeField] private GameObject platformPrefab;
        [SerializeField] private int minPoints = 2;
        [SerializeField] private int maxPoints = 5;
        [SerializeField] private int platformCount = 2;

        SpriteShapeController shape;
        float offsetY = 0.85f;

        public void InitPlatforms()
        {
            shape = GameplayManager.Get().tGenerate.shape;

            int index = 0;
            bool repeat = false;
            for (int i = 0; i < platformCount; i++)
            {
                do
                {
                    index = Random.Range(4, shape.spline.GetPointCount() - 5);

                    repeat = false;
                    foreach (int pIndex in GameplayManager.Get().platformIndex)
                    {
                        if (index == pIndex)
                        {
                            repeat = true;
                            break;
                        }
                    }
                } while (repeat);

                GameplayManager.Get().platformIndex.Add(index);
                int mPoints = Random.Range(minPoints, maxPoints);

                GameObject platformGO = Instantiate(platformPrefab);
                platformGO.name = "Platform " + (i + 1);

                platformGO.GetComponent<Platform>().UpdatePoints(mPoints);

                Vector3 auxPos = GameplayManager.Get().tGenerate.transform.position + shape.spline.GetPosition(index);
                auxPos.y += offsetY;

                if (mPoints >= 4 && mPoints <= 5)
                {
                    platformGO.transform.localScale = platformGO.transform.localScale * 80 / 100;
                    auxPos.y -= offsetY * 20 / 100;
                }

                platformGO.transform.position = auxPos;
                platformGO.transform.parent = transform;

                GameplayManager.Get().tGenerate.ChangeShapePosition(index, index + 1);
                GameplayManager.Get().tGenerate.ChangeShapePosition(index, index - 1);
            }
        }
    }
}
