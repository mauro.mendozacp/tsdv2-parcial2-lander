﻿using System;
using UnityEngine;

namespace LunarLander
{
    [Serializable]
    public class PlayerData
    {
        public float moveSpeed;
        public float turnSpeed;
        public float fuelBase;
        public float gravity = 0.25f;
        private float fuel = 0f;
        private int score = 0;
        private float altitude = 0f;
        private float velocityH = 0f;
        private float velocityV = 0f;
        
        [HideInInspector] public Rigidbody2D rigid;
        [HideInInspector] public float groundCheckDistance;
        [HideInInspector] public int multiplyPoints = 1;

        public static Action<float, float> receiveFuelEvent;
        public static Action<int> receiveScoreEvent;
        public static Action<float> receiveAltitudeEvent;
        public static Action<float> receiveVelocityHEvent;
        public static Action<float> receiveVelocityVEvent;


        public float Fuel
        {
            get => fuel;
            set
            {
                if (value < 0f)
                {
                    fuel = 0f;
                }
                else
                {
                    fuel = value;
                    receiveFuelEvent?.Invoke(fuel, fuelBase);
                }
            }
        }

        public int Score
        {
            get => score;
            set
            {
                if (value < 0)
                {
                    score = 0;
                }
                else
                {
                    score = value;
                    receiveScoreEvent?.Invoke(score);
                }
            }
        }

        public float Altitude
        {
            get => altitude;
            set
            {
                altitude = value;
                receiveAltitudeEvent?.Invoke(altitude);
            }
        }

        public float VelocityH
        {
            get => velocityH;
            set
            {
                velocityH = value;
                receiveVelocityHEvent?.Invoke(velocityH);
            }
        }

        public float VelocityV
        {
            get => velocityV;
            set
            {
                velocityV = value;
                receiveVelocityVEvent?.Invoke(velocityV);
            }
        }
    }
}