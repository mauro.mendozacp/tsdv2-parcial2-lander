﻿using UnityEngine;

namespace LunarLander
{
    public class Parallax : MonoBehaviour
    {
        [SerializeField] private GameObject[] levels;
        [SerializeField] private GameObject stars;
        [SerializeField] private PlayerController player;

        private Vector3 lastScreenPosition;

        void Start()
        {
            lastScreenPosition = player.transform.position;
        }

        void LateUpdate()
        {
            foreach (GameObject obj in levels)
            {
                float diff = lastScreenPosition.y - player.transform.position.y;
                obj.transform.position = new Vector3(obj.transform.position.x, diff * obj.transform.position.z, obj.transform.position.z);
            }
            UpdateStarsLayers();
        }

        public void UpdateStarsLayers()
        {
            Transform[] children = stars.GetComponentsInChildren<Transform>();
            if (children.Length > 1)
            {
                GameObject firstChild = children[1].gameObject;
                GameObject lastChild = children[children.Length - 1].gameObject;
                float halfChildHeight = lastChild.GetComponent<SpriteRenderer>().bounds.extents.y;
                if (player.transform.position.y + halfChildHeight * 2 > lastChild.transform.position.y + halfChildHeight)
                {
                    firstChild.transform.SetAsLastSibling();
                    firstChild.transform.position = new Vector3(lastChild.transform.position.x, lastChild.transform.position.y + halfChildHeight * 2, lastChild.transform.position.z);
                }
                else if (player.transform.position.y - halfChildHeight * 2 < firstChild.transform.position.y - halfChildHeight)
                {
                    lastChild.transform.SetAsFirstSibling();
                    lastChild.transform.position = new Vector3(firstChild.transform.position.x, firstChild.transform.position.y - halfChildHeight * 2, firstChild.transform.position.z);
                }
            }
        }
    }
}