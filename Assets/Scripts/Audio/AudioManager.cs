﻿using System;
using UnityEngine;
using UnityEngine.Audio;

namespace LunarLander
{
    public class AudioManager : MonoBehaviourSingleton<AudioManager>
    {
        public AudioMixerGroup mixer;
        [Range(0f, 1f)]
        public float volume;
        public Sound[] sounds;

#pragma warning disable CS0114 // El miembro oculta el miembro heredado. Falta una contraseña de invalidación
        void Awake()
#pragma warning restore CS0114 // El miembro oculta el miembro heredado. Falta una contraseña de invalidación
        {
            base.Awake();

            foreach (Sound s in sounds)
            {
                s.source = gameObject.AddComponent<AudioSource>();
                s.source.outputAudioMixerGroup = mixer;
                s.source.clip = s.clip;
                s.source.pitch = s.pitch;
                s.source.loop = s.loop;

                if (s.volume > volume)
                    s.source.volume = volume;
                else
                    s.source.volume = s.volume;
            }

            Play("music");
        }

        public void Play(string name)
        {
            Sound s = Array.Find(sounds, sound => sound.name == name);
            if (s == null)
            {
                Debug.LogWarning("Sound: " + name + " not found!");
                return;
            }
            s.source.Play();
        }

        public void Stop(string name)
        {
            Sound s = Array.Find(sounds, sound => sound.name == name);
            if (s == null)
            {
                Debug.LogWarning("Sound: " + name + " not found!");
                return;
            }
            s.source.Stop();
        }
    }
}