﻿using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace LunarLander
{
    public class GameManager : MonoBehaviourSingleton<GameManager>
    {
        public int Level { get; set; } = 1;
        public float Fuel { get; set; } = 0;
        public int Score { get; set; } = 0;
        public bool Win { get; set; } = false;
        public bool Finish { get; set; } = false;
        public bool SkipIntro { get; set; } = false;
        public string HighscorePath { get; } = "highscore.dat";

        public enum SceneGame
        {
            MainMenu,
            GamePlay
        }

        public void ChangeScene(SceneGame scene)
        {
            string sceneName;

            switch (scene)
            {
                case SceneGame.MainMenu:
                    sceneName = "MainMenu";
                    break;
                case SceneGame.GamePlay:
                    LoaderManager.Get().LoadScene("GamePlay");
                    return;

                default:
                    throw new ArgumentOutOfRangeException(nameof(scene), scene, null);
            }

            SceneManager.LoadScene(sceneName);
        }

        public List<int> GetScores()
        {
            List<int> hsList = new List<int>();
            FileStream fs;

            if (File.Exists(HighscorePath))
            {
                fs = File.OpenRead(HighscorePath);
                BinaryReader br = new BinaryReader(fs);

                while (br.BaseStream.Position != br.BaseStream.Length)
                {
                    hsList.Add(br.ReadInt32());
                }

                fs.Close();
                br.Close();

                hsList.Sort();
                hsList.Reverse();
            }

            return hsList;
        }

        public void Restart()
        {
            Level = 1;
            Score = 0;
            Fuel = 0;
            Win = false;
            Finish = false;
            ChangeScene(SceneGame.MainMenu);
        }

        public void NextLevel()
        {
            Level++;
            Win = false;
            Finish = false;
            ChangeScene(SceneGame.GamePlay);
        }

        public void FinishLevel(PlayerData player, bool win)
        {
            Score = player.Score;
            Fuel = player.Fuel;
            Win = win;
            Finish = true;
        }
    }
}