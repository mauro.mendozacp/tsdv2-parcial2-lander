﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace LunarLander
{
    public class FinishLevelUI : MonoBehaviour
    {
        [SerializeField] private TMP_Text highscoreText;
        [SerializeField] private TMP_Text resultText;
        [SerializeField] private TMP_Text scoreText;
        [SerializeField] private GameObject highScoreButton;
        [SerializeField] private GameObject nextLevelButton;
        [SerializeField] private GameObject backMenuButton;
        [SerializeField] private GameObject loadingPanel;

        string path;

        public void SetInfo()
        {
            path = GameManager.Get().HighscorePath;
            
            if (GameManager.Get().Win)
            {
                AudioManager.Get().Play("win");
                resultText.text = "LEVEL " + GameManager.Get().Level + " PASS";
                nextLevelButton.SetActive(true);
            }
            else
            {
                AudioManager.Get().Stop("music");
                if (GameManager.Get().Score != 0)
                {
                    List<int> hsList = GameManager.Get().GetScores();
                    if (hsList.Count > 0)
                    {
                        if (GameManager.Get().Score > hsList[0])
                        {
                            highscoreText.text = "NEW HIGHSCORE!";
                        }
                        else
                        {
                            highscoreText.text = "HIGHSCORE " + hsList[0];
                        }
                    }
                    else
                    {
                        highscoreText.text = "NEW HIGHSCORE!";
                    }

                    HighScore();
                }

                resultText.text = "GAME OVER";
                highScoreButton.SetActive(true);
                backMenuButton.SetActive(true);
            }

            scoreText.text = "Score: " + GameManager.Get().Score + "pts";
        }

        public void HighScore()
        {
            FileStream fs;

            if (!File.Exists(path))
            {
                fs = File.Create(path);
            }
            else
            {
                fs = File.Open(path, FileMode.Append);
            }

            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(GameManager.Get().Score);

            fs.Close();
            bw.Close();
        }

        public void NextLevel()
        {
            AudioManager.Get().Play("click");
            loadingPanel.SetActive(true);
            GameManager.Get().NextLevel();
        }

        public void BackToMenu()
        {
            AudioManager.Get().Play("click");
            AudioManager.Get().Play("music");
            GameManager.Get().Restart();
        }
    }
}