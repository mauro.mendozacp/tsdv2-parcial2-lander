﻿using UnityEngine;

namespace LunarLander
{
    public class PauseUI : MonoBehaviour
    {
        [SerializeField] private GameObject pauseGO;
        [SerializeField] private GameObject controlsGO;

        public void Resume()
        {
            AudioManager.Get().Play("click");
            GameplayManager.Get().pause = false;
            Time.timeScale = 1f;
            gameObject.SetActive(false);
        }

        public void ShowControls()
        {
            AudioManager.Get().Play("click");
            pauseGO.SetActive(false);
            controlsGO.SetActive(true);
        }

        public void ShowPause()
        {
            AudioManager.Get().Play("click");
            pauseGO.SetActive(true);
            controlsGO.SetActive(false);
        }

        public void BackToMenu()
        {
            AudioManager.Get().Play("music");
            AudioManager.Get().Play("click");
            Time.timeScale = 1f;
            GameManager.Get().Restart();
        }
    }
}