﻿using System.Collections.Generic;
using UnityEngine;

namespace LunarLander
{
    public class Splash : MonoBehaviour
    {
        [SerializeField] List<GameObject> splashes = new List<GameObject>();
        [SerializeField] private GameObject blackFade;

        private Animator anim;
        private int index = 0;

        void Awake()
        {
            blackFade.SetActive(true);
            foreach (GameObject s in splashes)
            {
                s.SetActive(false);
            }
        }

        void Start()
        {
            if (GameManager.Get().SkipIntro)
            {
                index = splashes.Count - 1;
                splashes[splashes.Count - 1].SetActive(true);
            }

            splashes[index].SetActive(true);
            anim = GetComponent<Animator>();
            FadeSplash();
        }

        public void FadeSplash()
        {
            anim.SetTrigger("Fade");
        }

        public void FadeFinish()
        {
            if (index + 1 < splashes.Count)
            {
                splashes[index].SetActive(false);
                splashes[index + 1].SetActive(true);

                index++;

                if (index + 1 < splashes.Count)
                {
                    FadeSplash();
                }
            }
        }

        public void DestroyBlackFade()
        {
            if (index + 1 >= splashes.Count)
            {
                blackFade.SetActive(false);
                GameManager.Get().SkipIntro = true;
            }
        }
    }
}