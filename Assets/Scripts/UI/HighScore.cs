﻿using System.Collections.Generic;
using UnityEngine;

namespace LunarLander
{
    public class HighScore : MonoBehaviour
    {
        [SerializeField] private GameObject highScoreGO;
        [SerializeField] private GameObject finishLevelGO;
        [SerializeField] private TMPro.TMP_Text[] hsTexts = new TMPro.TMP_Text[10];
        private string[] hsTextPref = { "1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th", "9th", "10th" };

        public void ShowHighScore()
        {
            AudioManager.Get().Play("click");
            finishLevelGO.SetActive(false);
            highScoreGO.SetActive(true);

            List<int> hsList = GameManager.Get().GetScores();

            for (int i = 0; i < hsList.Count; i++)
            {
                if (i >= hsTexts.Length)
                {
                    break;
                }

                hsTexts[i].text = hsTextPref[i] + ": " + hsList[i];
            }
        }

        public void BackInfo()
        {
            AudioManager.Get().Play("click");
            finishLevelGO.SetActive(true);
            highScoreGO.SetActive(false);
        }
    }
}