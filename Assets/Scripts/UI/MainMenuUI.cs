﻿using UnityEngine;

namespace LunarLander
{
    public class MainMenuUI : MonoBehaviour
    {
        [SerializeField] private GameObject menuPanel;
        [SerializeField] private GameObject creditsPanel;
        [SerializeField] private GameObject loadingPanel;

        public void PlayGame()
        {
            AudioManager.Get().Play("click");
            loadingPanel.SetActive(true);
            GameManager.Get().ChangeScene(GameManager.SceneGame.GamePlay);
        }

        public void ShowCredits()
        {
            AudioManager.Get().Play("click");
            menuPanel.SetActive(false);
            creditsPanel.SetActive(true);
        }

        public void ShowMenu()
        {
            AudioManager.Get().Play("click");
            menuPanel.SetActive(true);
            creditsPanel.SetActive(false);
        }

        public void ExitGame()
        {
            AudioManager.Get().Play("click");
            Application.Quit();
        }
    }
}