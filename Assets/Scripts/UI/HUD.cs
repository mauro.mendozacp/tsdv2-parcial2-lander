﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace LunarLander
{
    public class HUD : MonoBehaviour
    {
        [SerializeField] private TMP_Text timerText;
        [SerializeField] private TMP_Text scoreText;
        [SerializeField] private TMP_Text altitudeText;
        [SerializeField] private TMP_Text velocityHText;
        [SerializeField] private TMP_Text velocityVText;
        [SerializeField] private Image barImage;

        void Start()
        {
            PlayerData.receiveFuelEvent += SetBar;
            PlayerData.receiveScoreEvent += SetScoreText;
            PlayerData.receiveAltitudeEvent += SetAltitudeText;
            PlayerData.receiveVelocityHEvent += SetVelocityHText;
            PlayerData.receiveVelocityVEvent += SetVelocityVText;
        }

        void Update()
        {
            SetTimerText();
        }

        void OnDestroy()
        {
            PlayerData.receiveFuelEvent -= SetBar;
            PlayerData.receiveScoreEvent -= SetScoreText;
            PlayerData.receiveAltitudeEvent -= SetAltitudeText;
            PlayerData.receiveVelocityHEvent -= SetVelocityHText;
            PlayerData.receiveVelocityVEvent -= SetVelocityVText;
        }

        public void PauseGame()
        {
            AudioManager.Get().Play("click");
            GameplayManager.Get().PauseGame();
        }

        void SetTimerText()
        {
            int minutes = (int) (GameplayManager.Get().timer / 60);
            int seconds = (int) (GameplayManager.Get().timer % 60);

            timerText.text = minutes + ":";
            if (seconds < 10)
                timerText.text += "0";
            timerText.text += seconds;
        }

        void SetBar(float energy, float energyBase)
        {
            barImage.fillAmount = energy / energyBase;
        }

        void SetScoreText(int score)
        {
            scoreText.text = score + "pts";
        }

        void SetAltitudeText(float altitude)
        {
            altitudeText.text = (int)altitude + "m";
        }

        void SetVelocityHText(float velH)
        {
            velocityHText.text = (int)velH + "m/s";
        }

        void SetVelocityVText(float velV)
        {
            velocityVText.text = (int)velV + "m/s";
        }
    }
}